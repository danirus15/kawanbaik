

if($(window).width() > 767)
  {   
      /*$(document).ready(function(){
        var w = window.innerWidth;
        var h = window.innerHeight;
        h2 = h - 200;
        $('#home').css('min-height', h);
        $('#hubungi').css('min-height', h2);
      });*/
      //alert("kurang");
      function init() {
          window.addEventListener('scroll', function(e){
              var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                  shrinkOn = 520,
                  header = document.querySelector("#header");
              if (distanceY > shrinkOn) {
                  classie.add(header,"smaller");
              } else {
                  if (classie.has(header,"smaller")) {
                      classie.remove(header,"smaller");
                  }
              }
          });
      }
      window.onload = init();
      /*S:MODALBOX*/
      if( $('div.pop_box').attr('id') == 'pop_box_now'){
          $("body").css('overflow','hidden');
      }
      else {
        $("body").css('overflow','scroll');
      }

      $(".box_modal").click(function (){
        if( $('div').attr('id') == 'pop_box_now'){}
        else{ 
          var src  = $(this).attr("alt");
          size   = src.split('|');
              url      = size[0],
              width    = size[1],
              height   = size[2],
              tops   = 'calc(50% - '+ (height/2) +'px)';
              tops2  = '-webkit-calc(50% - '+ (height/2) +'px)';

          $("body").append( "<div class='pop_box' id='pop_box_now'><iframe frameborder='0' id='framebox' src=''></iframe></div>" );
          $("#framebox").animate({
            height: height,
            width: width,
          },0).attr('src',url).css('top',tops).css('top',tops2);
          $("body").css('overflow','hidden');
        }
      });

      $(window).load(function(){
        $(".box_modal_full").click(function (){
          if( $('div').attr('id') == 'pop_box_now'){}
          else{ 
            $(this).removeAttr('href');
            var src  = $(this).attr("alt");
            size   = src.split('|');
            url      = size[0],
            width    = '100%',
            height   = '100%'
        
            $("body").append( "<div class='pop_box' id='pop_box_now'><iframe frameborder='0' id='framebox' src=''></iframe></div>" );
            $("#framebox").animate({
              height: height,
              width: width,
            },0).attr('src',url).css('position','fixed').css('top','0').css('left','0');
            $("body").css('overflow','hidden');
          }
          rescale();
        });
        $(function() {
          $(".pop_container").wrapInner( "<div id='pop_wrap'></div>" );
          $('#pop_wrap').css('height',$(window).height());
          $('#pop_wrap').css('width',$('#pop_wrap').parent('.pop_container').width());
        });
        function rescale(){
          var size = {width: $(window).width() , height: $(window).height() }
          $('#pop_wrap').css('height', size.height );
        }
        $(window).bind("resize", rescale);
        $(".box_modal2").click(function (){
          $("#pop_box2").show();
          $("body").css('overflow','hidden');
        });
        function pop_next(src){
          size   = src.split('|');
          url      = size[0],
          width    = size[1],
          height   = size[2],
          tops   = 'calc(50% - '+ (height/2) +'px)';
          tops2  = '-webkit-calc(50% - '+ (height/2) +'px)';
          $("#framebox").animate({
            height: height,
            width: width,
          },0).attr('src',url).css('top',tops).css('top',tops2);
        };
      });

      function closepop()
      { 
        $("#pop_box_now").remove();
        $("#pop_box2").hide();
        $("body").css('overflow','scroll');
      };
      $(".close_box").click(function (){
        closepop();
      });
      $(".close_box_in").click(function (){
        parent.closepop();
      });
      $(".pop_next").click(function (){
        var src = $(this).attr("alt");
        parent.pop_next(src);
      });
      /*S:MODALBOX*/


      
  }
  else
  {
    $('.ico-menu').click(function() {
        $('#nav').show();
        $('.overlay').show();
        $('.arrow').show();
        $('#header').addClass("active");
        $('body').css("overflow","hidden");
    });
    $('#nav a').click(function() {
        //alert();
        $('#nav').hide();
        $('.overlay').hide();
        $('.arrow').hide();
        $('#header').removeClass("active");
        $('body').css("overflow","scroll");
    });
    // $('#nav').mouseout(function() {
    //     $('#nav').hide();
    //     $('.overlay').hide();
    //     $('.arrow').hide();
    //     $('#header').removeClass("active");
    //     $('body').css("overflow","scroll");
    // });
  }

  if($(window).height() < 766) {
    // Cache selectors
    var lastId,
          topMenu = $("#nav"),
          topMenuHeight = topMenu.outerHeight()+15,
          // All list items
          menuItems = topMenu.find("a"),
          // Anchors corresponding to menu items
          scrollItems = menuItems.map(function(){
            var item = $($(this).attr("href"));
            if (item.length) { return item; }
          });


      // Bind to scroll
      $(window).scroll(function(){
         // Get container scroll position
         var fromTop = $(this).scrollTop()+topMenuHeight;
         
         // Get id of current scroll item
         var cur = scrollItems.map(function(){
           if ($(this).offset().top < fromTop)
             return this;
         });
         // Get the id of the current element
         cur = cur[cur.length-1];
         var id = cur && cur.length ? cur[0].id : "";
         
         if (lastId !== id) {
             lastId = id;
             // Set/remove active class
             menuItems
               .parent().removeClass("selected")
               .end().filter("[href=#"+id+"]").parent().addClass("selected");
         }                   
      });
  }
  else {
    $( document ).ready(function() {
    $('#nav a').click(function() {
      //alert();
      $('#nav li').removeClass("selected");
      $(this).parent().addClass("selected");

    });
  });
  }



$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      target2 = $(this.hash);
      var hashtag = $(this).attr("href");
      
      if (target.length) {
          //alert(hashtag);
          $('html,body').animate({
            scrollTop: target.offset().top
          }, 700);
          /*$('html,body').animate({
            margin-
          }, 700);*/
      if($(window).width() > 768)
      {   
          $("section" + hashtag).css("padding-top", "70px");
        }
        else{
          $("section" + hashtag).css("padding-top", "50px");
        }

          return false;

      }
    }
  });
});


  


/*S:MODALBOX*/


/*S:MODALBOX*/

